package com.pasarmuid.user.util;

public interface ViewNotifier {
    void onViewNotify();
    void onProductDetailClick(int position);
}
