package com.pasarmuid.user.util;

import com.pasarmuid.user.modelclass.NewPendingDataModel;

import java.util.ArrayList;

public interface MyPendingReorderClick {
    void onReorderClick(ArrayList<NewPendingDataModel> models);
}
