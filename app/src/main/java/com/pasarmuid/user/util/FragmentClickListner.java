package com.pasarmuid.user.util;

public interface FragmentClickListner {
    void onFragmentClick(boolean open);
    void onChangeHome(boolean open);
}
