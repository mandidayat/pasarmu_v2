package com.pasarmuid.user.util;

public interface CallToDeliveryBoy {

    void onCallToDeliveryBoy(String number);
}
