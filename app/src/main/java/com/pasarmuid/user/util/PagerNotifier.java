package com.pasarmuid.user.util;

public interface PagerNotifier {
    void onPageNotifier(boolean value,int position);
}
