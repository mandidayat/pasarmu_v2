package com.pasarmuid.user.util;

import java.text.NumberFormat;
import java.util.Locale;

public class Utility {

    public static String format_rp(double rupiah) {
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        String convert;
        convert = formatRupiah.format(rupiah);
        return convert;
    }

}
