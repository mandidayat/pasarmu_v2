package com.pasarmuid.user.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pasarmuid.user.R;
import com.pasarmuid.user.activity.CategoryPage;
import com.pasarmuid.user.activity.CategorySubPage;
import com.pasarmuid.user.modelclass.HomeCate;
import com.pasarmuid.user.modelclass.SubCatModel;
import com.pasarmuid.user.util.CategoryFragmentClick;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.pasarmuid.user.config.BaseURL.IMG_URL;

public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.MyViewHolder> {

    private Context context;
    private List<SubCatModel> homeCateList;
    private CategoryFragmentClick categoryFragmentClick;
    private Random rnd = null;
    private RecyclerTouchListener listener;

    public interface RecyclerTouchListener {
        public void onClickItem(String titel, int position);

        public void onLongClickItem(View v, int position);
    }


    public SubCategoryAdapter(List<SubCatModel> homeCateList, Context context) {
        this.homeCateList = homeCateList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_sub_cat, parent, false);
        enableRnd();
        return new MyViewHolder(itemView);
    }

    private void enableRnd() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            try {
                rnd = SecureRandom.getInstanceStrong();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        } else {
            rnd = new Random();
        }
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        int currentColor = Color.rgb(255, 255, 255);
        holder.linearLayout.setBackgroundColor(currentColor);
        SubCatModel cc = homeCateList.get(position);
        holder.prodNAme.setText(cc.getName());
        holder.pdetails.setText(cc.getDetail());
        holder.image.setImageResource(R.drawable.splashicon);
        Picasso.get()
                .load(IMG_URL + cc.getImages()).networkPolicy(NetworkPolicy.NO_CACHE,NetworkPolicy.NO_STORE)
                .placeholder(R.drawable.splashicon)
                .into(holder.image);
        if (cc.getSubArray() == null || cc.getSubArray().length() == 0) {
            holder.pimage.setVisibility(View.GONE);
        }

        holder.cardView.setOnClickListener(v -> {
            Intent intent = new Intent(context, CategoryPage.class);
            intent.putExtra("cat_id", cc.getId());
            context.startActivity(intent);
//            if (categoryFragmentClick != null) {
//                categoryFragmentClick.onClick(cc.getId());
//            }
        });
    }

    @Override
    public int getItemCount() {
        return homeCateList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView prodNAme;
        TextView pdetails;
        ImageView image;
        ImageView pimage;
        ImageView image1;
        boolean mines = true;
        RecyclerView recyclerSubCate;
        LinearLayout cardView;
        RelativeLayout linearLayout;

        public MyViewHolder(View view) {
            super(view);
            prodNAme = view.findViewById(R.id.pNAme);
            pdetails = view.findViewById(R.id.pDetails);
            pimage = view.findViewById(R.id.image);
            image1 = view.findViewById(R.id.image1);
            image = view.findViewById(R.id.Pimage);
            recyclerSubCate = view.findViewById(R.id.recyclerSubCate);
            cardView = view.findViewById(R.id.cardView);
            linearLayout = view.findViewById(R.id.ll1);
        }
    }

}
