package com.pasarmuid.user.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pasarmuid.user.R;
import com.pasarmuid.user.modelclass.SearchProductModel;
import com.pasarmuid.user.util.Utility;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.List;

import static com.pasarmuid.user.config.BaseURL.IMG_URL;

public class SearchProdcutAdapter extends RecyclerView.Adapter<SearchProdcutAdapter.SearchProductView> {

    private List<SearchProductModel> searchList;
    private DecimalFormat dFormat;

    public class SearchProductView extends RecyclerView.ViewHolder {
        TextView title,txt_pName,txt_pInfo,txt_Pprice,txt_Mrp;
        ImageView prodImage;

        public SearchProductView(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.pNAme);
            txt_pName = (TextView) view.findViewById(R.id.txt_pName);
            txt_pInfo = (TextView) view.findViewById(R.id.txt_pInfo);
            txt_Pprice = (TextView) view.findViewById(R.id.txt_Pprice);
            txt_Mrp = (TextView) view.findViewById(R.id.txt_Mrp);
            prodImage = (ImageView) view.findViewById(R.id.prodImage);
        }
    }


    public SearchProdcutAdapter(List<SearchProductModel> searchList) {
        this.searchList = searchList;
    }

    @NonNull
    @Override
    public SearchProductView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_searchlist, parent, false);
        dFormat = new DecimalFormat("#.##");
        return new SearchProductView(itemView);
    }

    @Override
    public void onBindViewHolder(SearchProductView holder, int position) {
        SearchProductModel ss = searchList.get(position);
        holder.title.setText(ss.getpNAme());
        holder.txt_pName.setText(ss.getpNAme());
        holder.txt_pInfo.setText(ss.getSubprodList().getDescription());
        holder.txt_Pprice.setText(Utility.format_rp(Double.parseDouble(ss.getSubprodList().getPrice())));
        holder.txt_Mrp.setText(Utility.format_rp(Double.parseDouble(ss.getSubprodList().getMrp())));
        Picasso.get()
                .load(IMG_URL + ss.getSubprodList().getVarientImage()).networkPolicy(NetworkPolicy.NO_CACHE,NetworkPolicy.NO_STORE)
                .into(holder.prodImage);
    }

    @Override
    public int getItemCount() {
        return searchList.size();
    }
}
