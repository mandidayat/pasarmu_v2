package com.pasarmuid.user.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pasarmuid.user.Categorygridquantity;
import com.pasarmuid.user.R;
import com.pasarmuid.user.adapters.AdapterPopup;
import com.pasarmuid.user.adapters.CategoryGridAdapter;
import com.pasarmuid.user.config.BaseURL;
import com.pasarmuid.user.modelclass.NewCategoryDataModel;
import com.pasarmuid.user.modelclass.NewCategoryVarientList;
import com.pasarmuid.user.util.AppController;
import com.pasarmuid.user.util.CustomVolleyJsonRequest;
import com.pasarmuid.user.util.DatabaseHandler;
import com.pasarmuid.user.util.SessionManagement;

import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CategoryPage extends AppCompatActivity {

    RecyclerView recyclerProduct;
    CategoryGridAdapter adapter;
    List<NewCategoryDataModel> newCategoryDataModel = new ArrayList<>();
    String catId;
    String image;
    String title;
    private BottomSheetBehavior<View> behavior;
    private List<NewCategoryVarientList> varientProducts = new ArrayList<>();
    private LinearLayout bottomLayTotal;
    private TextView totalCount;
    private TextView totalPrice;
    private SessionManagement sessionManagement;
    private DatabaseHandler dbcart;
    EditText txtSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_page);
        sessionManagement = new SessionManagement(CategoryPage.this);
        recyclerProduct = findViewById(R.id.recycler_product);
        bottomLayTotal = findViewById(R.id.bottom_lay_total);
        totalPrice = findViewById(R.id.total_price);
        totalCount = findViewById(R.id.total_count);
        txtSearch = findViewById(R.id.txtSearch);
        TextView continueTocart = findViewById(R.id.continue_tocart);
        LinearLayout bottomSheet = findViewById(R.id.bottom_sheet);
        LinearLayout back = findViewById(R.id.back);
        behavior = BottomSheetBehavior.from(bottomSheet);
        catId = getIntent().getStringExtra("cat_id");
        image = getIntent().getStringExtra("image");
        title = getIntent().getStringExtra("title");
        dbcart = new DatabaseHandler(CategoryPage.this);
        back.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.putExtra("open", false);
            setResult(24, intent);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                finishAndRemoveTask();
            } else {
                finish();
            }
        });
        Categorygridquantity categorygridquantity = new Categorygridquantity() {
            @Override
            public void onClick(View view, int position, String ccId, String id) {
                varientProducts.clear();
                TextView txt = findViewById(R.id.txt);
                txt.setText(id);
                LinearLayout cancl = findViewById(R.id.cancl);
                cancl.setOnClickListener(v -> behavior.setState(BottomSheetBehavior.STATE_COLLAPSED));
                RecyclerView recylerPopup = findViewById(R.id.recyclerVarient);
                recylerPopup.setLayoutManager(new LinearLayoutManager(CategoryPage.this));
                varientProducts.addAll(newCategoryDataModel.get(position).getVarients());
                AdapterPopup selectCityAdapter = new AdapterPopup(CategoryPage.this, varientProducts, id, position1 -> {
                    if (varientProducts.get(position1).getVarientId().equalsIgnoreCase(newCategoryDataModel.get(position).getVarientId())) {
                        adapter.notifyItemChanged(position);
                    }
                });
                recylerPopup.setAdapter(selectCityAdapter);
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }

            @Override
            public void onCartItemAddOrMinus() {
//                if (dbcart.getCartCount() > 0) {
//                    bottomLayTotal.setVisibility(View.VISIBLE);
//                    totalPrice.setText(sessionManagement.getCurrency() + " " + dbcart.getTotalAmount());
//                    totalCount.setText("Total Items " + dbcart.getCartCount());
//                } else {
//                    bottomLayTotal.setVisibility(View.GONE);
//                }
            }

            @Override
            public void onProductDetials(int position) {
                Intent intent = new Intent(CategoryPage.this, ProductDetails.class);
                intent.putExtra("sId", newCategoryDataModel.get(position).getProductId());
                intent.putExtra("sName", newCategoryDataModel.get(position).getProductName());
                intent.putExtra("descrip", newCategoryDataModel.get(position).getDescription());
                intent.putExtra("price", newCategoryDataModel.get(position).getPrice());
                intent.putExtra("mrp", newCategoryDataModel.get(position).getMrp());
                intent.putExtra("unit", newCategoryDataModel.get(position).getUnit());
                intent.putExtra("qty", newCategoryDataModel.get(position).getQuantity());
                intent.putExtra("stock", newCategoryDataModel.get(position).getStock());
                intent.putExtra("image", newCategoryDataModel.get(position).getVarientImage());
                intent.putExtra("sVariant_id", newCategoryDataModel.get(position).getVarientId());
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivityForResult(intent,21);
            }
        };


        recyclerProduct.setLayoutManager(new GridLayoutManager(this, 1));
        adapter = new CategoryGridAdapter(newCategoryDataModel, CategoryPage.this, categorygridquantity);
        recyclerProduct.setAdapter(adapter);

        continueTocart.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.putExtra("open", true);
            setResult(24, intent);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                finishAndRemoveTask();
            } else {
                finish();
            }
        });
//        if (dbcart.getCartCount() > 0) {
//            bottomLayTotal.setVisibility(View.VISIBLE);
//            totalPrice.setText(sessionManagement.getCurrency() + " " + dbcart.getTotalAmount());
//            totalCount.setText("Total Items (" + dbcart.getCartCount() + ")");
//        } else {
//            bottomLayTotal.setVisibility(View.GONE);
//        }

        product(catId);

        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
//                if (txtSearch.getText().length() > 2) {
                    adapter.getFilter().filter(txtSearch.getText().toString());
//                }
            }
        });

    }

    private void product(String catIds) {
        newCategoryDataModel.clear();
        Map<String, String> params = new HashMap<>();
        params.put("cat_id", catIds);
        params.put("lat", sessionManagement.getLatPref());
        params.put("lng", sessionManagement.getLangPref());
        params.put("city", sessionManagement.getLocationCity());

        CustomVolleyJsonRequest jsonObjReq = new CustomVolleyJsonRequest(Request.Method.POST,
                BaseURL.CAT_PRODUCT, params, response -> {
            try {
                String status = response.getString("status");
                if (status.contains("1")) {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<NewCategoryDataModel>>() {
                    }.getType();
                    List<NewCategoryDataModel> listorl = gson.fromJson(response.getString("data"), listType);
                    newCategoryDataModel.addAll(listorl);
                    adapter.notifyDataSetChanged();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, error -> {
            error.printStackTrace();
            VolleyLog.d("", "Error: " + error.getMessage());
        });
        jsonObjReq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 90000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 0;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {
                error.printStackTrace();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjReq);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 21 && adapter!=null){
            adapter.notifyDataSetChanged();
        }
    }
}
