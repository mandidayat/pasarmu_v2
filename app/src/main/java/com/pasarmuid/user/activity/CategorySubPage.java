package com.pasarmuid.user.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;

import com.pasarmuid.user.R;
import com.pasarmuid.user.adapters.SubCatAdapter;
import com.pasarmuid.user.adapters.SubCategoryAdapter;
import com.pasarmuid.user.modelclass.SubCatModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CategorySubPage extends AppCompatActivity {

    private List<SubCatModel> subCatModels = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_sub_page);
        Bundle b = getIntent().getExtras();
        String Array=b.getString("json");
        try {
            JSONArray array = new JSONArray(Array);
            for (int i = 0; i < array.length(); i++) {
                try {
                    JSONObject object = array.getJSONObject(i);
                    SubCatModel model = new SubCatModel();
                    model.setDetail(object.getString("description"));
                    model.setId(object.getString("cat_id"));
                    model.setImages(object.getString("image"));
                    model.setName(object.getString("title"));
                    if (object.has("subchild")) {
                        model.setSubArray(object.getJSONArray("subchild"));
                    }
                    subCatModels.add(model);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            LinearLayout back = findViewById(R.id.back);
            back.setOnClickListener(v -> {
                Intent intent = new Intent();
                intent.putExtra("open", false);
                setResult(24, intent);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    finishAndRemoveTask();
                } else {
                    finish();
                }
            });
            SubCategoryAdapter cateAdapter = new SubCategoryAdapter(subCatModels, this);
            RecyclerView recyclerView = findViewById(R.id.recycler_product);
            recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(cateAdapter);
            cateAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}